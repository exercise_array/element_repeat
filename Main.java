import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void findElementRepeat(int[] arr, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j]){
                    System.out.println(arr[i]);
                    return;
                }
            }
        }
        System.out.println("NO");
    }

public static void findElementRepearSorted(int[] arr, int n) {
    Arrays.sort(arr);
    for (int i = 0; i < n - 1; i++) {
        if (arr[i] == arr[i+ 1]){
            System.out.println(arr[i]);
            return;
        }
    }
    System.out.println("NO");
}

    public static void findElementRepeatHash(int[] arr, int n) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < n; i++) {
            if(set.contains(arr[i])){
                System.out.println(arr[i]);
                return;
            } else {
                set.add(arr[i]);
            }
        }
        System.out.println("NO");
    }
    public static void main(String[] args) {
        int[] arr = {1, 4, 5, 7, 4, 9, 5};
        findElementRepeat(arr, arr.length);
        findElementRepearSorted(arr, arr.length);
        findElementRepeatHash(arr, arr.length);
    }

}
